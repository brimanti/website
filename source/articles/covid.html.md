---
title: What happens to employees in the midst of COVID-19? (Indonesian Law)
date: 2020-03-27 23:09:00 UTC
layout: article
---

The unprecedented situation of COVID-19 creates major impact towards employment
matters. As the government is drafting a regulation to regulate the lockdown
procedures, many companies have taken health and safety measures, among others,
by sending its employees to work from home.

<!-- READ MORE -->

It would not be a surprise if the number of termination of
employment or employment dispute raises due to this issue. Specifically, for
companies in the business sectors of, among others, restaurants, airlines, retails,
hotel, tourism. Recently, the Minister of Manpower has released a Cirular
Letter No. M/3/HK/04/III/2020 dated 17 March 2020 ("MOM Circular Letter") to the all governors to set
measures to prevent the COVID-19 pandemics in the workplace. One of the measures
is to instruct the head of companies to establish supportive measures for its
employees. This article will primarily discuss the rights of the
employees due to the current pandemic.

In legal term, pandemic or disease outbreak is commonly defined as "Force
Majeure" which means unforseen circumstances beyond the control of the parties
that can prevent a party to perform its obligation based on the contract.
It includes Act of Gods (natural disasters), war, riot, embargo, etc.

# The rights of sick employees

What if an employee is infected by Corona virus? Basically, regardless the type of
sickness, Law No.13 of 2003 ("Indonesian Employment Law") regulates that the
company cannot immediately terminate the sick employees as long as the period
of absent does not exceed of 12 (twelve) consecutive months and the company
has to pay salary of sick employees as follows (see Article 93 (3) Indonesian
Employment law).

  Article 93 (1)  The amount of salary payable to workers who are taken ill shall
  be determined as follows:
  a. 100% (one hundred percent) of the salary in the first 4 (four) months;
  b. 75% (seventy five percent) of the salary in the second 4 (four) months;
  c. 50% (fifty percent) of the salary in the third 4 (four) months;
  d. 25% (twenty five percent) of the salary before entrepreneurs terminates
      the employment.

Note that the employees have to provide a doctor statement to prove their sickness to the company.

# The impact of working from home

Depending on the nature of the work, not all employees are able to work from
home. When it is possible, it is logical that the employees could expect the
company to provide the facility to make it doable, such as laptop and mobile phone.
Usually it is not a big issue. Note that if the employees receive
transportation allowance to work, the company may not pay this at the moment,
as it is obvious no travel to the office is required. Other than that,
the company has to pay all remaining remuneration and benefits.

When it is not possible, typically the company will send the employees home (suspension).
Please see the sub-section below.

# Salary cut due to suspension

For example, most of employees who work in retail business are sent home
because of the company has to close its stores temporarily due to the
pandemic. Given their type of work (eg cashier, sales assistant),
they can't just simply work from home. For this issue, MOM Circular Letter
states that the amount of salary and the method of payment must be agreed
between the company and the employee. In this case, the company may propose a
salary reduction to employees. However, this does not include any mandatory
contribution by the company (eg the social security contribution). It is
advisable for the company to have a record of such agreement to avoid any
dispute in the future.

# Termination of employment

Many companies suffer major loss from this pandemic. One of the common
strategies to stop the bleeding is by cutting the number of employees. It is
very unfortunate situation but inevitable. For this topic, it is divided by
two type of employees, (i) Permanent employees; and (ii) fixed-term
employees.

For permanent employees, the company can argue that the termination is triggered by
the force majeure (ie COVID-19 pandemic). Consequently, the company has to
pay 1 (one) time of severance package as follows. Unless, the employee is
still under probation term. (see Article 164 (1) Indonesian Employment Law).

  Article 164
  (1) the entrepreneur may terminate the employment of his or her workers
  because of the enterprise has to be closed down due to continual losses it
  suffers for two years consecutively or force majeure. If this happens, the
  workers shall be entitled to severance pay amounting to 1 (one) time the
  amount of severance pay stipulated under subsection (2) of Article 156,
  reward pay for period of employment amounting to 1 (one) time the amount
  stipulated under sub-section (3) of Article 156 and compensation pay for
  entitlements that have not been used according to what is stipulated under
  subsection (4) of Article 156.

For termination of fixed term employee, the company has to pay the
salary of the employees for the remaining term of the contract. Unless, in the
employment contract it is stated that force majeure is one of the reason of
termination of employment. Please note that foreign workers (non-Indonesian national)
shall be deemed fixed-term employees.

# Disclaimer

Please be aware that it is necessary to check the provisions in the Company Regulations or
Collaborative Labor Agreement in the company, as applicable. It may specify greater details for
the relevant situations, for example separation payment that must be paid by the
company. The purpose of this article is to provide
general information. This article does not constitute a legal advice or opinion
whatsoever. I recommend seeking specific legal advice.