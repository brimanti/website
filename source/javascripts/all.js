(function() {
    "use strict";

    document.addEventListener('DOMContentLoaded', function() {
        let button = document.querySelector('.expand-menu a');

        if ( button ) {
            button.addEventListener('click', function(event) {
                event.preventDefault();

                document
                    .querySelector('.main-menu')
                    .classList
                    .toggle('mobile-visible');
            });
        }
    });
})();
